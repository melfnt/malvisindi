
# malvisindi

[🇮🇹 Leggi questa guida in italiano | Read this guide in italian.](https://bitbucket.org/melfnt/malvisindi/src/master/README_it.md)

This repository contains the teaching material for a dissemination laboratory created by the [Italian Association for Computational Linguistics](https://www.ai-lc.it/en/) (AILC) and used to popularize Natural Language Processing at several Italian science festivals and open-days events.

Here is a list of events in which the laboratory was presented, each time in a slightly different version depending on the audience and on the duration of the laboratory:

| Year | Event | Laboratory | City |
|------|-------|------------|------|
| 2021 | [Science Web Festival](https://www.sciencewebfestival.it/) | [Ehi Siri, che cos’è la Linguistica Computazionale? (Ehy Siri, what is the computational linguistics?)](https://www.sciencewebfestival.it/index.php/2021/04/09/ehi-siri-che-cose-la-linguistica-computazionale/) | Online |
| 2020 | [Researcher's night](https://nottedeiricercatori.pisa.it/programma-2020/) | Ehi Siri, che cos’è la Linguistica Computazionale? (Ehy Siri, what is the computational linguistics?) | Pisa (online) |
| 2020 | Alternanza scuola-lavoro [ITS Tullio Buzzi](https://www.tulliobuzzi.edu.it/) | Ehi Siri, che cos’è la Linguistica Computazionale? (Ehy Siri, what is the computational linguistics?) | Prato (online) |
| 2020 | [Festival della Scienza](http://www.festivalscienza.it/site/home.html) | [Il linguaggio di Siri](http://www.festivalscienza.it/site/home/programma-2020/programma-scuole/il-linguaggio-di-siri.html) (siri's language) | Genova (online) |
| 2020 | [SISSA student day](https://www.sissa.it/news/sissa-student-day-returns-thursday-13-february) | Ehi Siri, che cos’è la Linguistica Computazionale? (Ehy Siri, what is the computational linguistics?) | Trieste |
| 2019 | [BergamoScienza](http://www.bergamoscienza.it/) festival | [Non dire málvísindi se non l'hai nel sacco](http://www.bergamoscienza.it/it/calendario/53605/non-dire-m-lv-sindi-se-non-l-hai-nel-sacco) (don't jump the málvísindi) | Bergamo |


The hard-copy materials (corpora, cards and dictionaries in pdf format) are available in the `out/` folder and they can be downloaded, printed and used 'as is'. In case you wish to replicate the laboratory with a different corpus you have to download the whole repository, create two files in the `in/` folder and run the scripts in the `code/` folder to automatically generate the hard-copy material. 

The teaching material in the `out/` folder is distributed under the CC BY-NC-SA license, while the source code of the scripts in the `code` folder is distributed under the GNU gpl license version 3.

If you have any idea to improve the laboratory, the material and the code that generates it, if you have any questions or if you want to organize a dissemination acrivity together with us, please get in touch: Lucio Messina [luciotuttominuscolo@inventati.org](mailto:luciotuttominuscolo@inventati.org).

### Used technologies and prerequisites

All the scripts are written in [python](https://python.org): to use them you have to install python (version 3.6 or above) on your computer.

The generation of the pdf files is composed of several steps:

1. The script reads an input tsv file and generates a [latex](https://www.latex-project.org/) file. 
2. The script compiles the latex source into a pdf file using lualatex.
3. (only for the card generation) the script rearranges the pages of the pdf files using [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) and [pdfnup](https://github.com/rrthomas/pdfjam) such that they can be double-sided printed.

To run the scripts is thus necessary to install `python3`, `lualatex`, `pdftk` and `pdfnup` on the computer. All the latex sources and the auxiliary files are stored into the `tmp` folder (which is not tracked by git) and they are not deleted after the compilation. In this way it is possible to edit and recompile the latex source if needed.

On the Debian-based Operating Systems (Debian, Ubuntu, Linux Mint...) it is possible to install all the required packages using the command: 

    sudo apt install python3 texlive-latex-extra pdftk pdfjam

## Editing the input file and laboratory set up.

The preparation of the activity consists of 6 steps:

1. creating and tagging the corpora; 
2. choosing the words (and their tags) to be printed on the cards; 
3. checking the corpus and the chosen words;
4. generation of the files for printing;
5. printing corpora, cards, dictionaries, and clear-text corpora;
6. creating the material employed for the identification of phrases.

Here is a detailed explanation of each step.

### 1. creating and tagging the corpora

The corpus to be used for the activity has to be specified in an input tsv file that contains all the sentences, the parts of speech and the grammatical annotations: for an example of correctly formatted input file see `in/Biancaneve_en.tsv`. Since the grammatical annotations are complex, the representation of each sentence spans over multiple lines of the input file: the sequence of lines that corresponds to a single sentence is called block. Here is an example of block taken from the corpus:

        1	s	i	i	i	i	i	i	i	i	i				
                                    v	i	i	i				
            p	i	i	i				p	i	i				
                n	i	i	n	i			n	i				
            E	R	A	S	R	S	V	E	R	S				
            in	a	snowy	day	a	queen	was sewing	by	her	window

Since it is difficult to work on the tsv files using a text editor, we suggest to use a spreadsheet editor (like google documents or libreoffice) to create or edit the corpus, and then export your work into a tsv file. This is a screenshot of the same block edited using google docs:

![screenshot gdocs](https://bitbucket.org/melfnt/malvisindi/raw/13a50b319379ca7a9585dc775790a33e62684c40/doc/screenshot_gdocs_en.png)

Each line of a block is composed of a number of columns equal to the number of tokens of the sentence increased by two: in the example there are ten tokens in the sentence, thus each line is composed of twelve columns. The last line contains two empty columns followed by the tokens of the sentence, one per column. The penultimate line of the block contains two empty columns followed by the parts of speech of the tokens, one per column (line five of the example block reads `E` which is the part of speech of `in`, followed by `R` which is the part of speech of `a`... and so on until to the last `S` which is the part of speech of `window`). The other lines contain the grammatical structure of the sentence: each constituent takes up one of more columns of the same line and it is denoted by a letter depending on the constituent type placed in the first column, possibly followed by one or many `i`s placed in the further columns. The first line of each block contains an empty column, followed by the sentence number (in the example block is the 1 at line 1), followed by an `s`, followed by many `i`s on all the other columns, to denote that the whole sentence is a constituent of kind `s`. All the other constituents are placed in the lines between the second and the third to last one, aligned under the parent constituent and above the child constituents and tokens. In the example block, the sentence is composed of three constituents: a `p` constituent (`in a snowy day`), followed by an `n` constituent (`a queen`), followed by a `v` constituent (`was sewing by her window`). Since the first one is composed of four tokens (`in a snowy day`), it is written using a `p` followed by three `i`s (in the example these letters are placed at the beginning of line 3): the `p` denotes the kind of constituent and it is aligned above the first token (`in`), while the `i`s have to be aligned above the other three tokens. This constituent is, in turn, composed of the word `in` whose part of speech is `E`, followed by an `n` constituent (`a snowy day`), denoted by the `n` and the two `i`s at line 4 aligned under the `i`s at line 3.

Between a constituent and its children (either other constituents or parts of speech), some empty lines can be placed in order to "make room" for the other constituents, which representations possibly take up more lines. In the example, the `v` constituent (`was sewing by her window`) takes up three lines to be represented, while the first `p` constituent (`in a snowy day`) only takes up two: since all the tokens of the sentence must be "on the same level" at line 6, we left the second line empty above the `p` constituent.

Immediately after the block that represents the first sentence another block follows, representing the second sentence and so on for all the sentences in the corpus: the blocks will have a different number of rows (depending on the complexity of the phrases) and a different number of columns (depending on the lengths of the sentences). The corpus file has to be saved into the `in/` folder, in the next paragraphs we assume that its filename is `in/custom_corpus.tsv`.

### 2. choosing the words (and their tags) to be printed on the cards

In this second stage, the tokens to be used during the first (i.e. the one based on bigrams distribution) and second activity (i.e. the one based on the extraction of grammatical rules) should be chosen. The tokens must be stored in a .tsv file: see `in/selezione_parole_2909.tsv` for an example. The first row of the .tsv file (header) is ignored, while each of the other rows corresponds to one of the selected tokens and consists of 6 columns:

1. A flag (1 or 0) indicating whether the token belongs to the corpus or not --this column is ignored.
2. A flag (1 or 0) indicating whether the token should be used for the first activity.
3. A flag (1 or 0) indicating whether the token should be used for the second activity.
4. The number of cards to be printed for the token.
5. The POS of the token.
6. The token itself.

In order to use one of the selected tokens in both activities, you only need to write 1 in both the second and third columns, without duplicating the row. The .tsv file shoud be saved in the `in/` folder. From now on, we will refer to the .tsv file as `in/tokens_for_cards.tsv`.

### 3. checking the corpus and the chosen words

This step allows you to check which sentences the workshop participants will be able to generate using the corpus and the tokens chosen for the cards. Formatting errors in the corpus and card files will also be reported.
From the `code` folder, run the `generate_text.py` using the corpus and the card files as input parameters, as follows:

    cd code
    python3 generate_text ../in/custom_corpus.tsv ../in/tokens_for_cards.tsv
    
Replace `../in/custom_corpus.tsv` and `../in/tokens_for_cards.tsv` with the proper names of the corpus file and the cards file respectively.

The `generate_text.py` will print:

- 10 randomly generated sentences using the tokens selected for the first activity along with the distribution of bigrams extracted from the corpus;
- 10 sentences randomly generated using the tokens selected for the second activity along with the grammar extracted from the corpus. 

If the output produced by `generate_text.py` is not adequate (e.g. meaningless sentences; sentences that belong to the corpus or highly repetitive), you can always change the corpus and/or the selected tokens and repeat the verification step. 

For more information about the execution details of the scripts, please see the paragraph [details about the scripts](#markdown-header-details-about-the-scripts).

### 4. generation of the files for printing

In order to create the .pdf files for printing, run `tsv_to_latex.py` and `tsv_to_latex_cleartext.py` from the `code` folder using the corpus and the card files as input parameters, as follows:

    cd code
    python3 tsv_to_latex.py ../in/custom_corpus.tsv -a ../in/tokens_for_cards.tsv
    python3 tsv_to_latex_cleartext ../in/custom_corpus.tsv
    
Replace `../in/custom_corpus.tsv` and `../in/tokens_for_cards.tsv` with the proper names of the corpus file and the cards file respectively.
    
The script will output the .pdf files in the `out` folder:

- `custom_corpus_letters_translated.pdf` the translated corpus, with POS tagging and syntactic annotation.
- `custom_corpus_plaintext.pdf` the original corpus, with POS tagging and syntactic annotation.
- `custom_corpus_cleartext.pdf` the original corpus, without syntactic annotation.
- `custom_corpus_letters_dict.pdf` a dictionary containing the translation for each tokens in the original corpus.
- `custom_corpus_letters_cards_activity_1.pdf` The cards for the first activity.
- `custom_corpus_letters_cards_activity_2.pdf` The cards for the second activity.

Each token in `custom_corpus_letters_cards_activity_1.pdf` and `custom_corpus_letters_cards_activity_2.pdf` will be printed one or more times depending on the number of cards that you specified in the fourth column of the input card file (`in/tokens_for_cards.tsv`).

For more information about the execution details of the scripts, please see [details about the scripts](#markdown-header-details-about-the-scripts).

### 5. Creating the material employed for the identification of phrases

In order to carry out the activity with the transliterated corpus, the file `custom_corpus_letters_translated.pdf` should be used as corpus, while `custom_corpus_cleartext.pdf` should be provided to participants at the end of the actvity to translate the corpus and `custom_corpus_letters_dict.pdf` should be used as a dictionary to translate the generated sentences.

If the activity is carried out with the non-transliterated corpus instead, `custom_corpus_plaintext.pdf` should be printed and used as corpus.

In any case, both the front and the back of cards should be printed for both activities (`custom_corpus_letters_cards_activity_1.pdf` and `custom_corpus_letters_cards_activity_2.pdf`): on one side the token is presented, while the other shows its transliteration.

### 6. creating the material employed for the identification of phrases

Cut out multiple cards for each color of the lines highlighting phrases in the file `custom_corpus_letters_translated.pdf`. These cards should be used as placeholders for phrases in sentences created using a grammar during the second activity.

## Details about the scripts

The scripts in the `code` folder automatically generate the pdf files of the corpus to be printed from input files in tsv format. All the files necessary for the creation of pdfs are in the `in` folder and can be modified both to use different corpus and to change the appearance of the generated pdfs. All output files are saved in the `out` folder. 

### Generating transliterated text

Corpus with transliterated text can be generated with the command:

    python3 tsv_to_latex.py [-d] input_file.tsv [input_file.tsv ...] [-a additional_tokens_file]
    
For an example of an input file see the paragraph [creating and tagging the corpora](#markdown-header-1-creating-and-tagging-the-corpora) and the file `in/Biancaneve_en.tsv`. This command generates, for each input file:

1. a `translated` pdf file which contains the same sentences as the input file. Each word in the original text is replaced with another word made up of random letters (for example "spapata"). Alternatively, if you specify the `-d` flag from the command line, a random sequence of DING is used to translate each word of the text (for example "◆📞✂🖤"). The grammatical structure of each sentence is represented by a few lines above the sentence itself. Letters specifying parts of speech are translated into numbers from zero to nine and displayed as superscripts after each word. This file is the corpus to be given to the participants of the workshop.
2. a `dict` file (in pdf and tsv format) which contains a sequence with transliterated characters from each word of the original text. This sequence is contained in the `translated` file. This file can be used by the animators at the end of the session to translate the sentences that the participants composed in each activity.
3. (only if the `-d` flag is specified) a `ding` pdf file that contains all used DINGs. The DING images represented in this file can be used to create activity material.
4. a `grammar` txt file containing the grammar rules extracted from the corpus. This file can help animators and animators to get an idea of which grammar rules are present in the corpus and their frequency.
5. a tsv `corpus` file that contains all the sentences of the corpus, one per line: on the first column of each line there is a sentence from the original file, on the second its translation.
6. a `plaintext` pdf file, similar to the `translated` file but containing the original sentences, grammar notation and untranslated parts of speech.

It is possible to specify the flag `-a additional_tokens.tsv` from the command line indicating a file containing additional tokens to be translated, which will then be used to create the cards for each activity of the laboratory. For information on the format of the additional token file see the paragraph [choosing the words (and their tags) to be printed on the cards](#markdown-header-2-choosing-the-words-and-their-tags-to-be-printed-on-the-cards) and the file `in/selezione_parole_2909.tsv` .

If the `-a` flag is specified, the script generates all the files described above, plus:

1. a tsv file `additional_tokens_1` containing all the additional tokens for the first activity and their translation: on the first column of each line there is the translation of the token, on the second the originale text.
2. a tsv file `additional_tokens_2` which contains all the additional tokens for the second activity and their translation: on the first column of each line there is the translation of the token, on the second the original text.
3. a pdf file `cards_activity_1` which contains the cards for the first activity to be printed double-sided (four cards per sheet). The front of each card specifies the token, the back its translation. Each card appears one or more times depending on the number of repetitions specified in the additional token file.
4. a `cards_activity_2` pdf file containing the cards for the second activity to be printed double-sided (four cards per sheet). On the front of each card is written a token and its part of the speech, on the back are the translations. Each card appears one or more times depending on the number of repetitions specified in the additional token file.

### Non-transliterated text generation

Clear-text corpora can be generated using the command:

```python3 tsv_to_latex_cleartext.py input_file.tsv```

This creates:

1. a `cleartext` pdf file that contains the same sentences as the input file, without any transliteration nor indication of the grammatical structure of the sentences themselves. This file should be given to participants at the end of the session to show them the correspondence between language formed by sequences of random words or DINGs and natural language.

### Extraction of bigram frequencies and text generation

The bigrams present in a corpus and their frequencies can be extracted with the command:

    python3 bigrams.py input_file.tsv

This command generates a `bigrams` tsv file which contains all the bigrams present in the original text, sorted by frequency. In the output file, the words `<sos>` and `<eos>` respectively indicate the beginning and end of a sentence. This file can help you prepare for the lab to choose which words to use in the Markov chain activity.

The same `bigrams` file can be used to generate some random text using the command:

    python3 generate_text.py bigrams.tsv

The script prints ten randomly generated sentences according to the probability distribution of the bigrams in the `bigrams.tsv` file.
This command can be useful before the activities to get an idea of the type of sentences that can be generated starting from the distribution of the corpus bigrams.

## References

We wrote a scientific article about the genesis and goals of the laboratory. The article also contains a detailed description of all the activities (including the ones for which the teaching material in this repository is not needed) and some reflections on how the laboratory was received by the participants: *Ludovica Pannitto, Lucia Busso, Claudia Roberta Combei, Lucio Messina, Alessio Miaschi, Gabriele Sarti, and Malvina Nissim. 2021. [Teaching NLP with bracelets and restaurant menus: An interactive workshop for italian students](https://arxiv.org/abs/2104.12422). In Proceedings of the Fifth Workshop on Teaching NLP and CL, Online event. Association for Computational Linguistics.*
