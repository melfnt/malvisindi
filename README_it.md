# malvisindi

[🇬🇧 Read this guide in English.](https://bitbucket.org/melfnt/malvisindi/src/master/README.md)

Questo repository contiene il materiale didattico per un laboratorio divulgativo creato dall'[Associazione Italiana di Linguistica Computazionale](https://www.ai-lc.it/) (AILC) e presentato in molti festival scientifici italiani e open-days.

Questo è un elenco degli eventi a cui il laboratorio è stato presentato, ogni volta in una versione leggermente diversa a seconda del pubblico e delle tempistiche:

| Anno | Evento | Laboratorio | Città |
|------|--------|-------------|-------|
| 2021 | [Science Web Festival](https://www.sciencewebfestival.it/) | [Ehi Siri, che cos’è la Linguistica Computazionale?](https://www.sciencewebfestival.it/index.php/2021/04/09/ehi-siri-che-cose-la-linguistica-computazionale/) | Online
| 2020 | [Notte dei ricercatori](https://nottedeiricercatori.pisa.it/programma-2020/) | Ehi Siri, che cos’è la Linguistica Computazionale? | Pisa (online) |
| 2020 | Alternanza scuola-lavoro [ITS Tullio Buzzi](https://www.tulliobuzzi.edu.it/) | Ehi Siri, che cos’è la Linguistica Computazionale? | Prato (online) |
| 2020 | [Festival della Scienza](http://www.festivalscienza.it/site/home.html) | [Il linguaggio di Siri](http://www.festivalscienza.it/site/home/programma-2020/programma-scuole/il-linguaggio-di-siri.html) | Genova (online) |
| 2020 | [SISSA student day](https://www.sissa.it/news/sissa-student-day-returns-thursday-13-february) | Ehi Siri, che cos’è la Linguistica Computazionale? | Trieste |
| 2019 | festival [BergamoScienza](http://www.bergamoscienza.it/) | [Non dire málvísindi se non l'hai nel sacco](http://www.bergamoscienza.it/it/calendario/53605/non-dire-m-lv-sindi-se-non-l-hai-nel-sacco) | Bergamo |


I materiali già pronti da stampare (corpora, carte e dizionari in formato pdf) si trovano nella cartella `out/` e possono essere semplicemente scaricati ed usati così come sono. Se invece si vuole realizzare la stessa attività con un corpus diverso da quello fornito bisogna scaricare tutto il repository, creare due file nella cartella `in/` e usare gli script nella cartella `code/` per generare automaticamente il materiale da stampare. 

Il materiale didattico nella cartella `out/` è distribuito secondo i termini della licenza CC BY-NC-SA, il codice sorgente degli script nella cartella `code` è distribuito secondo i termini della licenza GNU gpl versione 3.

Se hai qualche idea su come migliorare il laboratorio, il materiale e il codice che lo genera, o se hai dubbi, curiosità o vuoi organizzare un'attività di divulgazione insieme a noi contattaci: Lucio Messina [luciotuttominuscolo@inventati.org](mailto:luciotuttominuscolo@inventati.org).

### Tecnologie utilizzate e prerequisiti

Tutti gli script sono scritti in [python](https://python.org): per utilizzarli è necessario avere python (versione 3.6 o superiore) installata sul computer.

La generazione dei file pdf avviene in più passi:

1. lo script legge il file di input (in formato tsv) e genera un sorgente [latex](https://www.latex-project.org/).
2. lo script compila il sorgente latex in un file pdf usando lualatex.
3. (solo per la generazione delle carte) lo script riordina le pagine dei file pdf usando [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) e [pdfnup](https://github.com/rrthomas/pdfjam) in modo che possano essere stampate fronte-retro.

Per utilizzare gli script è quindi necessario installare `python3`, `lualatex`, `pdftk` e `pdfnup` sul computer. Tutti i sorgenti latex e i file ausiliari vengono salvati nella cartella `tmp` (non tracciata da git) e non vengono eliminati al termine della compilazione. In questo modo se serve è possibile modificare e ricompilare manualmente i sorgenti latex.

Sui sistemi basati su Debian (Debian, Ubuntu, Linux Mint...) è possibile installare tutti i pacchetti richiesti digitando il comando:

    sudo apt install python3 texlive-latex-extra pdftk pdfjam

## Modifica dei file di input e preparazione dell'attività.

Questi sono i passaggi da eseguire per preparare l'attività:

1. creazione e annotazione del corpus;
2. scelta delle parole per le carte;
3. verifica del corpus e delle parole scelte;
4. generazione dei file per la stampa;
5. stampa corpus, carte, dizionari e corpus in chiaro;
6. creazione dei cartoncini che identificano i sintagmi.

Ciascun punto spiegato nel dettaglio:

### 1. creazione e annotazione del corpus

Il corpus da usare per l'attività deve essere specificato in un file di input in formato tsv che contiene tutte le frasi, le parti del discorso e le annotazioni grammaticali: per un esempio di file di input correttamente formattato vedere il file `in/Biancaneve_it.tsv`. Siccome le annotazioni grammaticali sono complesse, ogni frase richiede più di una riga del file di input per essere rappresentata: la sequenza di righe che corrisponde a una singola frase si chiama blocco. Questo è un esempio di blocco preso dal corpus:

        1	s	i	i	i	i	i	i	i	i	i	i	i				
                                    v	i	i	i	i	i				
            p	i	i	i				p	i	p	i	i				
                n	i	i	n	i			n		n	i				
            E	R	S	A	R	S	V	E	S	E	R	S				
            in	un	giorno	nevoso	una	regina	cuciva	in	silenzio	a	la	finestra	

Siccome è difficile lavorare direttamente sui file tsv con un editor di testo, consigliamo di utilizzare un editor di fogli di calcolo (come google documenti o libreoffice) per modificare il corpus, e alla fine esportare il lavoro su un file tsv. Questo è uno screenshot dello stesso blocco aperto in google documenti:

![screenshot gdocs](https://bitbucket.org/melfnt/malvisindi/raw/1dbfb4c21ee043858c61b4069489fd887308c980/doc/screenshot_gdocs.png)

Ogni riga di un blocco è composta da un numero di colonne pari al numero di token della frase aumentato di due: nell'esempio la frase ha dodici token e ogni riga del blocco ha quattordici colonne. L'ultima riga contiene due colonne vuote seguite dai token che compongono la frase, uno per colonna. La penultima riga contiene due colonne vuote seguite dalle parti del discorso di ogni token che compone la frase, uno per colonna (nell'esempio alla riga cinque compare `E` che è la parte del discorso di `in`, `R` che è la parte del discorso di `un`... e così via fino all'ultima `S` che è la parte del discorso di `finestra`). Le altre righe contengono la struttura grammaticale della frase: ogni costituente occupa una o più colonne ed è indicato con la lettera corrispondente nella prima colonna, seguita da tante `i` nelle eventuali altre colonne. La prima riga di ogni blocco contiene una colonna vuota, seguita dal numero della frase (nell'esempio è 1 alla riga 1), seguita da una `s` e poi tante `i` su tutte le altre colonne, per indicare che l'intera frase è un costituente di tipo `s`. Tutti gli altri costituenti vengono indicati nelle righe comprese fra la seconda e la terzultima, incolonnati sotto ai costituenti di cui fanno parte e sopra ai token di cui sono composti. Nell'esempio l'intera frase è formata da tre costituenti: un costituente di tipo `p` (`in un giorno nevoso`), seguito da un costituente di tipo `n` (`una regina`), seguito da un costituente di tipo `v` (`cuciva in silenzio a la finestra`). Siccome il primo di questi tre costituenti è formato da 4 token (`in un giorno nevoso`), deve essere indicato con una `p` seguita da tre `i` (nell'esempio queste lettere sono riportate all'inizio della riga 3): la `p` indica il tipo di costituente e deve essere incolonnato con il primo token (`in`), mentre le `i` devono essere incolonnate con gli altri token. A sua volta questo costituente è composto da una parola la cui parte del discorso è `E` (`in`), seguita da un costituente di tipo `n` (`un giorno nevoso`), indicato alla riga 4 con una `n` e due `i` incolonnate sotto le `i` della terza riga. 

Fra un costituente e quelli che lo compongono, così come fra un costituente e le parti del discorso che lo compongono, può essere lasciata una o più righe vuote così da "fare spazio" agli altri costituenti più complessi che hanno bisogno di più righe per essere rappresentati. Nell'esempio, il costituente di tipo `v` (`cuciva in silenzio a la finestra`) ha bisogno di tre righe per essere rappresentato, mentre per il primo costituente di tipo `p` (`in un giorno nevoso`) bastano due righe: per far sì che la frase sia tutta "sullo stesso piano" a riga 6 abbiamo lasciato la seconda riga bianca sopra il costituente di tipo `p`.

Immediatamente sotto al blocco che rappresenta la prima frase segue il blocco che rappresenta la seconda e così via per tutte le frasi che compongono il corpus: ciascun blocco sarà formato da un diverso numero di righe (in base alla complessità dei sintagmi) e da un diverso numero di colonne (in base alla lunghezza delle frasi). Il file con il corpus va salvato nella cartella `in/`, nei prossimi paragrafi supporremo che il nome del file sia `in/custom_corpus.tsv`.

### 2. scelta delle parole per le carte

In questa fase bisogna scegliere i token da usare per la prima attività (quella sui bigrammi) e per la seconda attività (quella sulle grammatiche). Questi token devono essere specificati in un file tsv: per un esempio di file correttamente formattato vedere `in/selezione_parole_2909.tsv`. La prima riga del file è un'intestazione e viene ignorata, le altre righe rappresentano un token ciascuna e sono formate da 6 colonne:

1. Un flag (0 oppure 1) che indica se il token appartiene al corpus oppure no --questa colonna viene ignorata.
2. Un flag (0 oppure 1) che indica se il token deve essere usato per la prima attività. 
3. Un flag (0 oppure 1) che indica se il token deve essere usato per la seconda attività. 
4. Il numero di carte che devono essere stampate per questo token.
5. La parte del discorso del token.
6. Il token.

Per usare un token in entrambe le attività è sufficiente scrivere 1 sia nella seconda che nella terza colonna, senza dover duplicare la riga. Il file con i token per le carte va salvato nella cartella `in/`, nei prossimi paragrafi supporremo che il nome del file sia `in/tokens_for_cards.tsv`.

### 3. verifica del corpus e delle parole scelte

Questo passaggio serve per controllare quali frasi saranno in grado di generare i partecipanti del laboratorio usando il corpus e le parole scelte per le carte, verranno riportati anche eventuali errori di formattazione del file del corpus e delle carte.
Dalla cartella `code` lanciare lo script `generate_text.py` passando come parametri i file del corpus e dei token per le carte:

    cd code
    python3 generate_text ../in/custom_corpus.tsv ../in/tokens_for_cards.tsv
    
ovviamente bisogna sostituire `../in/custom_corpus.tsv` con il nome del file del corpus e `../in/tokens_for_cards.tsv` con il nome del file dei token per le carte. Lo script stampa:

- 10 frasi generate casualmente usando le parole selezionate per la prima attività e la distribuzione dei bigrammi del corpus;
- 10 frasi generate casualmente usando le parole selezionate per la seconda attività e la grammatica estratta dal corpus. 

Se i risultati non sono soddisfacenti (per esempio se lo script genera solo frasi che non hanno senso, appartenenti al corpus oppure molto ripetitive), è possibile cambiare il corpus e/o le parole selezionate e ripetere questa verifica.

Per maggiori informazioni sul funzionamento e le opzioni degli script, vedere il paragrafo [gli script in dettaglio](#markdown-header-gli-script-in-dettaglio).

### 4. generazione dei file per la stampa.

Dalla cartella `code` lanciare gli script `tsv_to_latex.py` e `tsv_to_latex_cleartext.py` passando come parametri i file del corpus e dei token per le carte:

    cd code
    python3 tsv_to_latex.py ../in/custom_corpus.tsv -a ../in/tokens_for_cards.tsv
    python3 tsv_to_latex_cleartext ../in/custom_corpus.tsv
    
Ovviamente bisogna sostituire `../in/custom_corpus.tsv` con il nome del file del corpus e `../in/tokens_for_cards.tsv` con il nome del file dei token per le carte.
Gli script generano i file da stampare in formato pdf nella cartella `out`: 

- `custom_corpus_letters_translated.pdf` il corpus traslitterato, con parti del discorso e annotazione sintattica.
- `custom_corpus_plaintext.pdf` il corpus in chiaro, con parti del discorso e annotazione sintattica.
- `custom_corpus_cleartext.pdf` il corpus in chiaro senza annotazione sintattica.
- `custom_corpus_letters_dict.pdf` un dizionario con l'associazione fra parole traslitterate e parole del corpus originale.
- `custom_corpus_letters_cards_activity_1.pdf` Le carte per la prima attività.
- `custom_corpus_letters_cards_activity_2.pdf` Le carte per la seconda attività.

In entrambi i file delle carte ogni token è ripetuto una o più volte a seconda del numero di ripetizioni specificato nella quarta colonna del file di input con i token per le carte.

Per maggiori informazioni sul funzionamento e le opzioni degli script, vedere il paragrafo [gli script in dettaglio](#markdown-header-gli-script-in-dettaglio).


### 5. stampa corpus, carte, dizionari e corpus in chiaro.

Se si vuole realizzare l'attività con il corpus traslitterato, stampare `custom_corpus_letters_translated.pdf` da usare come corpus, `custom_corpus_cleartext.pdf` da dare ai partecipanti alla fine delle attività per vedere la traduzione del corpus e `custom_corpus_letters_dict.pdf` da usare come dizionario per tradurre le frasi formate. Se invece si vuole realizzare l'attività con il corpus non traslitterato, stampare `custom_corpus_plaintext.pdf` da usare come corpus.

In ogni caso stampare fronte-retro le carte per entrambe le attività (`custom_corpus_letters_cards_activity_1.pdf` e `custom_corpus_letters_cards_activity_2.pdf`): da un lato c'è scritto il token, sull'altro lato la sua traslitterazione.

### 6. creazione dei cartoncini che identificano i sintagmi.

Ritagliare tanti cartoncini di ognuno dei colori delle linee che evidenziano i sintagmi nel file `custom_corpus_letters_translated.pdf`. Questi cartoncini vanno usati come segnaposto per i sintagmi delle frasi create usando la grammatica durante la seconda attività.

## Gli script in dettaglio.

Gli script nella cartella `code` generano automaticamente i pdf dei corpus da stampare a partire da file di input in formato tsv. Tutti i file necessari alla realizzazione dei pdf sono nella cartella `in` e possono essere modificati sia per usare corpus diversi che per cambiare l'aspetto dei pdf generati. Tutti i file di output vengono salvati nella carella `out`.

### Generazione del testo translitterato

I corpus con il testo translitterato possono essere generati con il comando:

    python3 tsv_to_latex.py [-d] input_file.tsv [input_file.tsv ... ] [-a additional_tokens_file]
    
Per un esempio di file di input vedere il paragrafo [creazione e annotazione del corpus](#markdown-header-1-creazione-e-annotazione-del-corpus) e il file `in/Biancaneve_it.tsv`. Questo comando genera, per ogni file di input:

1. un file pdf `translated` che contiene le stesse frasi del file di input. Ogni parola nel testo originale viene sostituita con un'altra parola formata da lettere casuali (per esempio "spapata"). In alternativa, se si specifica il flag `-d` dalla linea di comando, per tradurre ogni parola del testo viene usata una sequenza casuale di DING (per esempio "◆📞✂🖤"). La struttura grammaticale di ogni frase è rappresentata da alcune linee sopra la frase stessa. Le lettere che specificano le parti del discorso vengono tradotte in numeri da zero a nove e visualizzate come apice dopo ogni parola. Questo file è il corpus da dare ai partecipanti e alle partecipanti del laboratorio.
2. un file `dict` (in formato pdf e tsv) che contiene, per ogni parola del testo originale, la sequenza con cui è stata translitterata nel file `translated`. Questo file può essere usato dagli animatori e dalle animatrici al termine della sessione per tradurre le frasi che i partecipanti hanno composto in ogni attività.
3. (solo se si è specificato il flag `-d`) un file pdf `ding` che contiene tutti i DING utilizzati. Le immagini dei DING rappresentate in questo file possono essere utilizzate per creare il materiale delle attività.
4. un file txt `grammar` che contiene le regole grammaticali estratte dal corpus. Questo file può essere d'aiuto agli animatori e alle animatrici per farsi un'idea di quali regole grammaticali sono presenti nel corpus e della loro frequenza.
5. un file tsv `corpus` che contiene tutte le frasi del corpus, una per riga: sulla prima colonna di ogni riga c'è una frase del file originale, sulla seconda la sua traduzione.
6. un file pdf `plaintext`, simile al file `translated` ma che contiene le frasi originali, l'annotazione grammaticale e le pos non tradotte.

È possibile specificare dalla linea di comando il flag `-a additional_tokens.tsv` indicando un file di token aggiuntivi da tradurre, che verranno poi utilizzati per creare le carte da usare in ogni attività del laboratorio. Per informazioni sul formato del file di token aggiuntivi vedere il paragrafo [scelta delle parole per le carte](#markdown-header-2-scelta-delle-parole-per-le-carte) e il file `in/selezione_parole_2909.tsv`.

Se il flag `-a` viene specificato, lo script genera tutti i file descritti sopra, e in aggiunta:

1. un file tsv `additional_tokens_1` che contiene tutti i token aggiuntivi per la prima attività e la loro traduzione: sulla prima colonna di ogni riga c'è la traduzione del token, sulla seconda il testo originale.
2. un file tsv `additional_tokens_2` che contiene tutti i token aggiuntivi per la seconda attività e la loro traduzione: sulla prima colonna di ogni riga c'è la traduzione del token, sulla seconda il testo originale.
3. un file pdf `cards_activity_1` che contiene le carte per la prima attività da stampare fronte-retro (quattro carte per foglio). Sul fronte di ogni carta c'è scritto un token, sul retro la sua traduzione. Ogni carta compare una o più volte a seconda del numero di ripetizioni specificato nel file dei token aggiuntivi.
4. un file pdf `cards_activity_2` che contiene le carte per la seconda attività da stampare fronte-retro (quattro carte per foglio). Sul fronte di ogni carta c'è scritto un token e la sua parte del discorso, sul retro ci sono le rispettive traduzioni. Ogni carta compare una o più volte a seconda del numero di ripetizioni specificato nel file dei token aggiuntivi.

### Generazione del testo non translitterato

I corpus con il testo "in chiaro" può essere generato con il comando:

    python3 tsv_to_latex_cleartext.py input_file.tsv

Questo comando genera:

1. un file pdf `cleartext` che contiene le stesse frasi del file di input, senza nessuna traslitterazione e senza nessuna indicazione sulla struttura grammaticale delle frasi stesse. Questo file va dato ai partecipanti e alle partecipanti al termine della sessione per mostrare loro la corrispondenza fra linguaggio formato da sequenze di parole a caso o DING e linguaggio naturale.

### Estrazione delle frequenze dei bigrammi e generazione del testo

I bigrammi presenti in un corpus e le loro frequenze possono essere estratte con il comando:

    python3 bigrams.py input_file.tsv

Questo comando genera un file tsv `bigrams` che contiene tutti i bigrammi presenti nel testo originale, ordinati per frequenza. Nel file di output le parole `<sos>` ed `<eos>` indicano rispettivamente l'inizio e la fine di una frase. Questo file può essere d'aiuto durante la preparazione del laboratorio per scegliere quali parole utilizzare nell'attività sulle catene di Markov.

Lo stesso file `bigrams` può essere usato per generare del testo casuale usando il comando:

    python3 generate_text.py bigrams.tsv

Lo script stampa dieci frasi generate casualmente secondo la distribuzione di probabilità dei bigrammi nel file `bigrams.tsv`.
Questo comando può essere utile prima delle attività per avere un'idea del tipo di frasi che possono essere generate a partire dalla distribuzione dei bigrammi del corpus.

## Bibliografia

Abbiamo scritto un articolo scientifico (in inglese) in cui si trova una discussione sul motivo per cui abbiamo creato il laboratorio e i suoi obiettivi, la descrizione dettagliata di tutte le attività (comprese quelle per cui non è necessario il materiale didattico presente in questo repository) e alcune riflessioni su come è stato recepito dai partecipanti: *Ludovica Pannitto, Lucia Busso, Claudia Roberta Combei, Lucio Messina, Alessio Miaschi, Gabriele Sarti, and Malvina Nissim. 2021. [Teaching NLP with bracelets and restaurant menus: An interactive workshop for italian students](https://arxiv.org/abs/2104.12422). In Proceedings of the Fifth Workshop on Teaching NLP and CL, Online event. Association for Computational Linguistics.*
