'''
	usage:
		python generate_text.py bigrams_file.tsv
	
	given a file containing some bigrams and their frequencies, generates some random text according to the probability distribution of the bigrams.
	The bigram file can be generated out of a corpus tsv file using the script bigrams.py

	input format:
		# lines started with # are comments
		# one line per bigram, each line has three columns separated by \t: word1\tword2\tfrequency
		# for example:
		<sos>/    the/R    12
		the/R    queen/S   9
		<sos>/    snowwhite/S      8
		in/E    the/R      7
		<sos>/    she/P    7
		beautiful/A    unlike/E    7
		# and so on...
		# <sos>, <eos> mean start of sentence, end of sentence respectively

'''

import sys
import os
import random
from collections import defaultdict

def generate_sentence ( next_words, cum_weights ):
	word = "<sos>"
	count = 0
	sent = []
	while not word == "<eos>" and count < 200:
		sent.append (word)
		word = random.choices ( next_words[word], cum_weights=cum_weights[word], k=1 )[0]
		count += 1
	
	if word == "<eos>":
		return sent[1:]
	else: 
		return None


def main ( in_fname ):

	print ("in: {}, out: {}".format (in_fname, "stdout"))

	next_words = defaultdict (list)
	weights = {}
	with open(in_fname) as fin:
		for lineno, line in enumerate(fin,1):
			try:
				if not line.startswith ("#"):
					w1,w2,count = line.split('\t')
					count = int(count)
					w1, w2 = w1.split("/")[0], w2.split("/")[0]
					next_words[w1].append(w2)

					if w1 in weights:
						cumulative_weight = weights[w1][-1] + count
					else:
						cumulative_weight = count
						weights[w1] = []
					weights[w1].append (cumulative_weight)
			except Exception as e:
				raise ValueError ("Input format error at line {}: {}".format(lineno, line))

	if "<sos>" not in next_words:
		raise ValueError ("there must be at least one bigram starting with <sos>")
	
	for i in range (10):
		sent = generate_sentence (next_words, weights)
		if sent is not None:
			print (" ".join (sent))



if __name__ == "__main__":

	if len(sys.argv) < 2 or not os.path.isfile (sys.argv[1]):
		sys.exit ("No input file specified or input file is not a file. Please specify a bigrams tsv file")

	main (sys.argv[1])
