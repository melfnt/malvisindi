"""
 usage:
  python tsv_to_latex.py [-d] input_file.tsv [input_file.tsv ... ] [-a additional_tokens_file]

 for an example of input file see `../in/Biancaneve_it.tsv`

 if the -d option is provided, for each input tsv file, parses the text and produces several
           latex files in the folder ../tmp
        - a `traslated text` file containing the input text in which each word has been randomly replaced with a
            different sequence of DINGs.
            The syntax tree of each sentence is represented by several lines over the sentence text in the output file.
        - a `dict` file containing each original word and the sequence of DINGs that encodes it in the
            `translated text` file (both in pdf and tsv format).
        - a `ding` file containing all the used DING.
        - a `grammar` file containing all the grammar rules extracted from the corpus.
        - a `corpus` tsv file containing the original sentences and their translation.
        - a `plaintext` similar to the `translated` file but without the grammatical structure highlight. 

 if the -d option not is specified, the words are translated with other words rather than with DINGS and
 the `ding` file is not produced.

 All the latex files in the ../tmp folder are compiled using lualatex and the output pdf files are put
 in the ../out folder.
 The latex files and the auxiliary files in ../tmp are not deleted so that the user can possibly to edit and recompile
 them manually.

 If an additional tokens file is provided (-a switch) the tokens read from that file are also translated (one token
 per line).
 In this case some other files are produced in addition to the ones listed above:
        - two `additional token` tsv files (one per activity), containing the cleartext and translated version of the
          additional tokens.
        - two `cards` pdf files (one per activity), containing the cards with the tokens.
 for an example of additional token file see `../in/selezione_parole_2909.tsv`

 for the detailed documentation see `../README.md`
"""

import sys
import os
import random
import re
import argparse
from collections import namedtuple

# distance between words and the first line (pt)
_FIRST_LINE_DISTANCE = 4
# distance between lines (pt)
_INTRA_LINE_DISTANCE = 9
# pifont characters used to transliterate input characters.
_DINGS = [34, 37, 38, 40, 42, 46, 52, 54, 59, 61, 65, 68, 71, 72, 81, 95, 100, 108, 110, 115, 116, 117, 119, 161, 164,
          168, 169, 170, 171, 220, 228]

# do not break line after these sentences
# _DO_NOT_BREAK = [5, 7, 16, 18, 22, 26, 29, 31, 33, 35, 37, 39, 48, 52, 54, 57, 61]
_DO_NOT_BREAK = []

# distance between token and pos in pdf additional tokens (cm)
_TOKEN_POS_DISTANCE = 1

AdditionalToken = namedtuple("AdditionalToken", ["flag1", "flag2", "repetitions", "text", "pos"])


class TranslatorDings:
    def __init__(self, dings, initial_pos_list):
        self._pos_list = initial_pos_list

        self.map = {}
        self.i = 0
        self.dings = dings

        self.word_to_sym = {}
        self.sym_combinations = set()

        self.len_to_sym = [1, 1, 2, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7,
                           7, 7, 7, 7, 7, 7, 7, 7, 7]

        self.random_source = random.Random()
        self.random_source.seed(42)

    def __call__(self, c):
        if c in '.,!?:-':
            return c
        # print ("[DEBUG] tt(",c,") = ")
        if c in self.map:
            # print ("[DEBUG] in map:", self.map[c])
            return self.map[c]
        elif self.i < len(self.dings):
            self.map[c] = '\\ding{' + str(self.dings[self.i]) + '}'
            self.i += 1
            # print ("[DEBUG] next symbol:", self.map[c])
            return self.map[c]
        # print ("[DEBUG] no more symbols:", c)
        return c

    def translate(self, word):
        word = word.lower()
        # ~ print("translate", word)
        if word in self.word_to_sym:
            # ~ print("word in dict: {}".format(self.word_to_sym[word]))
            return self.word_to_sym[word]
        else:
            # ~ print(word, len(word))
            new_len = self.len_to_sym[len(word)]
            # ~ print("generating string of {} symbols".format(new_len))
            sym_combination = self.generate_combination(new_len)
            while sym_combination in self.sym_combinations:
                sym_combination = self.generate_combination(new_len)
            # ~ print("chosen combination: {}".format(sym_combination))
            self.word_to_sym[word] = sym_combination
            self.sym_combinations.add(sym_combination)
            return sym_combination

    def generate_combination(self, k):
        self.random_source.shuffle(self.dings)
        return ''.join("\\ding{" + str(x) + '}' for x in self.dings[:k])

    def print_dictionary(self, fout, separator="\t", extra_newline=True):
        for word, sym in sorted(self.word_to_sym.items(), key=lambda x: x[1]):
            print("{}{}{}{}".format(sym, separator, word, "\n" if extra_newline else ""), file=fout)

    def print_tsv_dictionary(self, fout, separator="\t", translate_only=None):
        print("DING{}CLEARTEXT".format(separator), file=fout)
        if not translate_only:
            for word, sym in sorted(self.word_to_sym.items(), key=lambda x: x[1]):
                sym = "".join(chr(int(x)) for x in re.findall("([0-9]+)", sym))
                print("{}{}{}".format(sym, separator, word), file=fout)
        else:
            for word in sorted(translate_only):
                sym = "".join(chr(int(x)) for x in re.findall("([0-9]+)", self.word_to_sym[word]))
                print("{}{}{}".format(sym, separator, word), file=fout)

    def print_translated_sentence(self, sentence, fout, separator="\t"):
        ts = " ".join(
            "".join(chr(int(x)) for x in re.findall("([0-9]+)", self.word_to_sym[w.lower()])) for w in sentence)
        sentence = " ".join(sentence)
        print("{}{}{}".format(ts, separator, sentence), file=fout)

    def print_pos_dictionary(self, fout):
        for pos, i in sorted((y, x) for x, y in enumerate(self._pos_list)):
            print("{}\t{}\n".format(pos, i), file=fout)

    def translate_pos(self, pos):
        # print ("[DEBUG] translate_pos(", pos, ")")
        if pos not in self._pos_list:
            # print ("[DEBUG] *********** NEW POS ************", pos)
            self._pos_list.append(pos)
        i = self._pos_list.index(pos)
        # print ("[DEBUG] ", str(i))
        return str(i)

class TranslatorEmpty:
    def __init__(self):
        self._pos_list = []

        self.map = {}
        self.i = 0

        self.word_to_sym = {}
        self.sym_combinations = set()

        self.len_to_sym = [1, 2, 3, 4, 4, 5, 5, 5, 6, 6, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9,
                           9, 9, 9, 9, 9]

        self.random_source = random.Random()
        self.random_source.seed(42)

    def __call__(self, c):
        if c in '.,!?:-':
            return c
        # print ("[DEBUG] tt(",c,") = ")
        if c in self.map:
            # print ("[DEBUG] in map:", self.map[c])
            return self.map[c]
        elif self.i < len(self.dings):
            # self.map[c] = '\\ding{' + str(self.dings[self.i]) + '}'
            self.map[c] = "a"
            self.i += 1
            # print ("[DEBUG] next symbol:", self.map[c])
            return self.map[c]
        # print ("[DEBUG] no more symbols:", c)
        return c

    def translate(self, word):
        word = word.lower()
        self.word_to_sym[word] = word
        return word

    def print_dictionary(self, fout, separator="\t", extra_newline=True):
        for word, sym in sorted(self.word_to_sym.items(), key=lambda x: x[1]):
            print("{}{}{}{}".format(sym, separator, word, "\n" if extra_newline else ""), file=fout)

    def print_tsv_dictionary(self, fout, separator="\t", translate_only=None):
        print("TRANSLATED{}CLEARTEXT".format(separator), file=fout)
        if not translate_only:
            for word, sym in sorted(self.word_to_sym.items(), key=lambda x: x[1]):
                print("{}{}{}".format(sym, separator, word), file=fout)
        else:
            for word in sorted(translate_only):
                print("{}{}{}".format(self.word_to_sym[word], separator, word), file=fout)

    def print_translated_sentence(self, sentence, fout, separator="\t"):
        ts = " ".join(self.word_to_sym[w.lower()] for w in sentence)
        sentence = " ".join(sentence)
        print("{}{}{}".format(ts, separator, sentence), file=fout)

    def print_pos_dictionary(self, fout):
        for pos, i in sorted((y, x) for x, y in enumerate(self._pos_list)):
            print("{}\t{}\n".format(pos, i), file=fout)

    def translate_pos(self, pos):
        # print ("[DEBUG] translate_pos(", pos, ")")
        if pos not in self._pos_list:
            # print ("[DEBUG] *********** NEW POS ************", pos)
            self._pos_list.append(pos)
        return pos


class TranslatorLetters:
    def __init__(self, dings, initial_pos_list):
        self._pos_list = initial_pos_list

        self.map = {}
        self.i = 0
        self.dings = dings

        self.word_to_sym = {}
        self.sym_combinations = set()

        self.len_to_sym = [1, 2, 3, 4, 4, 5, 5, 5, 6, 6, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9,
                           9, 9, 9, 9, 9]

        self.random_source = random.Random()
        self.random_source.seed(42)

    def __call__(self, c):
        if c in '.,!?:-':
            return c
        # print ("[DEBUG] tt(",c,") = ")
        if c in self.map:
            # print ("[DEBUG] in map:", self.map[c])
            return self.map[c]
        elif self.i < len(self.dings):
            # self.map[c] = '\\ding{' + str(self.dings[self.i]) + '}'
            self.map[c] = "a"
            self.i += 1
            # print ("[DEBUG] next symbol:", self.map[c])
            return self.map[c]
        # print ("[DEBUG] no more symbols:", c)
        return c

    def translate(self, word):
        word = word.lower()
        # ~ print("translate", word)
        if word in self.word_to_sym:
            # ~ print("word in dict: {}".format(self.word_to_sym[word]))
            return self.word_to_sym[word]
        else:
            #    print(word, len(word))
            new_len = self.len_to_sym[len(word)]
            #    print("generating string of {} symbols".format(new_len))

            sym_combination = self.generate_combination(new_len)

            while sym_combination in self.sym_combinations:
                sym_combination = self.generate_combination(new_len)
            #    print("chosen combination: {}".format(sym_combination))
            self.word_to_sym[word] = sym_combination
            self.sym_combinations.add(sym_combination)
            return sym_combination

    def generate_combination(self, k):

        word_structs = {1: ["v"],
                        2: ["vc", "cv"],
                        3: ["cvc", "vcv", "ccv"],
                        4: ["cvcv", "vcvc", "ccvc", "vccv"],
                        5: ["cvcvc", "vcvcv", "ccvcv", "cvccv", "vccvc"],
                        6: ["cvcvcv", "vcvcvc", "ccvccv", "ccvcvc", "vccvcv"],
                        7: ["cvcvcvc", "vcvcvcv", "vccvcvc", "ccvcvcv", "cvcvccv"],
                        8: ["cvcvccvc", "vcvcvccv", "vccvcvcv", "vccvccvc", "ccvccvcv", "vcvcvcvc"],
                        9: ["cvccvcvcv", "ccvcvccvc"]}

        vs = ["a", "e", "o", "y"]
        cs = ["p", "t", "m", "s", "l", "r", "k", "v", "c"]

        transitions = {
            "a": {"v": ["e", "y"], "c": cs},
            "e": {"v": ["a", "y"], "c": cs},
            # "i": {"v": ["a", "y"], "c": cs},
            "o": {"v": ["y"], "c": cs},
            "y": {"v": ["a", "e", "o"], "c": ["t", "m", "s", "l", "r", "k", "v", "c"]},
            "p": {"v": ["a", "e", "o"], "c": ["r", "l"]},
            # "p": {"v": vs, "c": ["r", "t"]},
            # "d": {"v": vs, "c": ["r"]},
            "t": {"v": vs, "c": ["m", "l"]},
            "m": {"v": vs, "c": ["p", "t"]},
            "s": {"v": vs, "c": ["p", "t", "m", "k", "v"]},
            # "z": {"v": vs, "c": ["b", "p", "d", "t", "l", "k"]},
            "l": {"v": vs, "c": ["p", "m", "k", "v", "c"]},
            "r": {"v": vs, "c": ["r", "l", "p", "t", "m", "v", "c"]},
            "k": {"v": vs, "c": ["r", "l"]},
            "v": {"v": vs, "c": ["r", "l"]},
            "c": {"v": ["e", "y"], "c": ["s", "r", "l"]}
        }


        s = self.random_source.choice(word_structs[k])

        s_translate = ''
        prev = ''
        for char in s:
            if prev:
                choice = self.random_source.choice(transitions[prev][char])
            else:
                if char == "v":
                    choice = self.random_source.choice(vs)
                else:
                    choice = self.random_source.choice(cs)

            s_translate += choice
            prev = choice

        return s_translate

    def print_dictionary(self, fout, separator="\t", extra_newline=True):
        for word, sym in sorted(self.word_to_sym.items(), key=lambda x: x[1]):
            print("{}{}{}{}".format(sym, separator, word, "\n" if extra_newline else ""), file=fout)

    def print_tsv_dictionary(self, fout, separator="\t", translate_only=None):
        print("TRANSLATED{}CLEARTEXT".format(separator), file=fout)
        if not translate_only:
            for word, sym in sorted(self.word_to_sym.items(), key=lambda x: x[1]):
                print("{}{}{}".format(sym, separator, word), file=fout)
        else:
            for word in sorted(translate_only):
                print("{}{}{}".format(self.word_to_sym[word], separator, word), file=fout)

    def print_translated_sentence(self, sentence, fout, separator="\t"):
        ts = " ".join(self.word_to_sym[w.lower()] for w in sentence)
        sentence = " ".join(sentence)
        print("{}{}{}".format(ts, separator, sentence), file=fout)

    def print_pos_dictionary(self, fout):
        for pos, i in sorted((y, x) for x, y in enumerate(self._pos_list)):
            print("{}\t{}\n".format(pos, i), file=fout)

    def translate_pos(self, pos):
        # print ("[DEBUG] translate_pos(", pos, ")")
        if pos not in self._pos_list:
            # print ("[DEBUG] *********** NEW POS ************", pos)
            self._pos_list.append(pos)
        i = self._pos_list.index(pos)
        # print ("[DEBUG] ", str(i))
        return str(i)


class Node(object):
    def __init__(self, is_leaf):
        self.is_leaf = is_leaf


class Leaf(Node):
    def __init__(self, pos, text):
        super().__init__(True)
        self.pos = pos
        self.text = text


class Parent(Node):
    def __init__(self, kind, children):
        super().__init__(False)
        self.kind = kind
        self.children = children


def print_grammar(T, depth=0, fout=sys.stdout):
    if T.is_leaf:
        return
    else:
        print("{} -> ".format(T.kind), end='', file=fout)

        first = True
        for c in T.children:
            if not first:
                print(" + ", end='', file=fout)
            if c.is_leaf:
                print("{}".format(c.pos), end='', file=fout)
            else:
                print("{}".format(c.kind), end='', file=fout)
            first = False

        print('', file=fout)
        for c in T.children:
            print_grammar(c, depth + 1, fout)


def print_tree(T, depth=0):
    print(' ' * depth, end='')
    if T.is_leaf:
        print("{}^{}".format(T.text, T.pos))
    else:
        print(T.kind)
        for c in T.children:
            print_tree(c, depth + 1)


def parse_row(M, i, jstart, jend):
    # print ("parse_row i={} jstart={} jend={}".format(i,jstart,jend))
    # input()

    ipos = len(M) - 2
    itext = len(M) - 1
    if i == ipos:
        # print ("end parse_row (base case, leafs) i={} jstart={} jend={}".format(i,jstart,jend))
        # print ("return", [ Leaf (M[ipos][j], M[itext][j]) for j in range (jstart, jend) ])
        return [Leaf(M[ipos][j], M[itext][j]) for j in range(jstart, jend)]

    children = []
    j1 = jstart
    kind = M[i][j1]  # '' or a valid kind
    for j in range(jstart + 1, jend):
        if not kind and M[i][j]:  # end of a '' row
            children += parse_row(M, i + 1, j1, j)
            # print ("resume parse_row i={} jstart={} jend={} after whitespace row parsing".format(i,jstart,jend))
            # print ("current index j={}".format(j))
            j1 = j
            kind = M[i][j1]
        elif kind and not M[i][j] == 'i':  # end of a valid kind row
            T = parse_tree_matrix(M, i, j1, j)
            # print ("resume parse_row i={} jstart={} jend={} after normal row parsing".format(i,jstart,jend))
            # print ("current index j={}".format(j))
            children.append(T)
            j1 = j
            kind = M[i][j1]
    if kind:
        T = parse_tree_matrix(M, i, j1, jend)
        children.append(T)
    else:
        children += parse_row(M, i + 1, j1, jend)

    # print ("end parse_row (children) i={} jstart={} jend={}".format(i,jstart,jend))
    # print ("return", children)
    return children


def parse_tree_matrix(M, i, jstart, jend):
    # print ("parse_tree_matrix i={} jstart={} jend={}".format(i,jstart,jend))
    # input()
    ipos = len(M) - 2
    itext = len(M) - 1
    if i == ipos:
        assert jend == jstart, "wrong input matrix given to parse_tree_matrix (or buggy algorithm ;)): " \
                               "jend must be jstart M={} i={} jstart={} jend={}".format(M, i, jstart, jend)
        # print ("end parse_tree_matrix i={} jstart={} jend={}".format(i,jstart,jend))
        # print ("return", Leaf (M[ipos][jstart], M[itext][jstart]))
        return Leaf(M[ipos][jstart], M[itext][jstart])
    assert not M[i][
                   jstart] == 'i', "could not parse_tree_matrix with start position in the beginning of a node M={} " \
                                   "i={} jstart={} jend={}".format(
        M, i, jstart, jend)
    assert all(x == 'i' for x in M[i][
                                 jstart + 1:jend]), "Non single node passed in input to parse_tree_matrix M={} " \
                                                    "i={} jstart={} jend={}".format(M, i, jstart, jend)
    children = parse_row(M, i + 1, jstart, jend)
    # print ("end parse_tree_matrix i={} jstart={} jend={}".format(i,jstart,jend))
    # print ("return", Parent (M[i][jstart], children))
    return Parent(M[i][jstart], children)


# yelds sentence_id, M, is_trailing_newline 
def parse_tsv(fname):
    with open(fname) as fin:
        M = []
        sentence_id = 0
        sentence_length = 0
        for c, line in enumerate(fin):
            line = line.rstrip()
            # print ("new line (stripped)", line)
            row = line.split('\t')
            # print ("row", row)
            # print ("row[0]", row[0])
            if row[1] and len(M):
                yield sentence_id, M, sentence_id not in _DO_NOT_BREAK
                # yield sentence_id, M, not row[0]
                M = []
            if not len(M):
                #                sentence_id = int(row[1])
                sentence_id += 1
                sentence_length = len(row) - 2
            row = row[2:]
            assert len(
                row) <= sentence_length, "wrong input sentence length at line {}: len(row) {} " \
                                         "sentence_length {}".format(c, len(row), sentence_length)
            if len(row) < sentence_length:
                row = row + [''] * (sentence_length - len(row))
            M.append(row)
            # print ("M", M)
        if len(M):
            yield sentence_id, M, sentence_id not in _DO_NOT_BREAK


def parse_additional_tokens(fname):
    with open(fname) as fin:
        fin.readline()
        for line in fin:
            if line.strip():
                corpus, f1, f2, n_repetitions, pos, text = line.rstrip().split('\t')
                yield AdditionalToken(bool(int(f1)), bool(int(f2)), int(n_repetitions), text, pos)


def tree_to_latex(root, translator):
    if root.is_leaf:
        # cadevano\textsuperscript{\color{red}V}
        # print ("[DEBUG]",T.text)
        arg = translator.translate(root.text) + "\\textsuperscript{\\color{red}\\textbf{" + translator.translate_pos(
            root.pos) + "}}"
        return 0, "{\\textcolor{black}{" + arg + "}}"
    else:
        subcalls_results = list(map(lambda x: tree_to_latex(x, translator), root.children))
        l = max(x[0] for x in subcalls_results) + 1
        pt = str(_FIRST_LINE_DISTANCE + (l + 1) * _INTRA_LINE_DISTANCE) + "pt"
        children_latex = " ".join(x[1] for x in subcalls_results)
        color = "color" + root.kind
        # {{\color{colorp}\UMOline{15pt}{ CHILDREN }}}
        pre = "{{\\color{" + color + "}\\UMOline{" + pt + "}{"
        post = "}}}"
        return l, pre + children_latex + post


def sentence_to_latex(root, translator, sentence_id, is_newline):
    suffix = "\\vspace{1cm}\n\n" if is_newline else "\\hspace{0.5cm}\n"
    prefix = "{\\color{red}\\LARGE\\textbf{" + str(sentence_id) + ".}} "
    return prefix + tree_to_latex(root, translator)[1] + "\n" + suffix


# given the name of a tex file in the ../tmp folder compiles it and optionally copies the pdf output in the ../out folder
def compile_pdf(latex_fname, do_copy=True):
    created_pdf_fname = latex_fname.replace(".tex", ".pdf")
    output_fname = created_pdf_fname.replace("../tmp/", "../out/")
    print("compiling latex file {}. output: {}".format(latex_fname, output_fname if do_copy else created_pdf_fname))
    os.system("lualatex -interaction=nonstopmode --output-directory=../tmp " + latex_fname + " | grep Overfull -A1")
    if do_copy:
        os.system("cp '{}' '{}'".format(created_pdf_fname, output_fname))


def generate_translate_text_and_grammar_and_corpus(fname, basename, suffix, translator):
    plaintext_fname = "../tmp/" + basename + "_plaintext.tex"
    translated_fname = "../tmp/" + basename + suffix + "_translated.tex"
    grammar_fname = "../out/" + basename + "_grammar.txt"
    corpus_fname = "../out/" + basename + suffix + "_corpus.tsv"

    plain_translator = TranslatorEmpty()

    print("in: ", fname, "out (translated text):", translated_fname)
    print("in: ", fname, "out (plaintext):", plaintext_fname)
    print("in: ", fname, "out (grammar):", grammar_fname)
    print("in: ", fname, "out (corpus):", corpus_fname)

    with open(translated_fname, "w") as fout, open(grammar_fname, "w") as grammar_fout, \
            open(corpus_fname, "w") as corpus_fout, open(plaintext_fname, "w") as fout_p:

        print("=== GRAMMAR ===", file=grammar_fout)
        print("TRANSLATED\tCLEARTEXT", file=corpus_fout)

        with open("../in/header.tex") as headerin:
            header = headerin.read()
            print(header, file=fout)
            print(header, file=fout_p)

        for sentence_id, M, is_newline in parse_tsv(fname):
            # print ("new matrix for sentence", sentence_id)
            # print ('\n'.join([str(x) for x in M]))
            # print ("\n")
            tree = parse_tree_matrix(M, 0, 0, len(M[0]))
            print_grammar(tree, 0, grammar_fout)
            # print_tree (T, 0)
            # print (sentence_id, ".", is_newline)
            s = sentence_to_latex(tree, translator, sentence_id, is_newline)
            s_p = sentence_to_latex(tree, plain_translator, sentence_id, is_newline)
            print(s, file=fout, end='')
            print(s_p, file=fout_p, end='')

            translator.print_translated_sentence(M[-1], corpus_fout)

        with open("../in/footer.tex") as footerin:
            footer = footerin.read()
            print(footer, file=fout)
            print(footer, file=fout_p)

    compile_pdf(translated_fname)
    compile_pdf(plaintext_fname)


def generate_dict(fname, basename, suffix, translator):
    dict_fname = "../tmp/" + basename + suffix + "_dict.tex"
    print("in: ", fname, "out (dict):", dict_fname)

    with open(dict_fname, "w") as fout:
        with open("../in/header_dict.tex") as headerin:
            print(headerin.read(), file=fout)

        print("=== WORD DICTIONARY ===\n", file=fout)
        translator.print_dictionary(fout)

        print("=== POS DICTIONARY ===\n", file=fout)
        translator.print_pos_dictionary(fout)

        with open("../in/footer_dict.tex") as footerin:
            print(footerin.read(), file=fout)

    compile_pdf(dict_fname)

    dict_fname_tsv = "../out/" + basename + suffix + "_dict.tsv"
    print("in: ", fname, "out (tsv dict):", dict_fname_tsv)
    with open(dict_fname_tsv, "w") as fout:
        translator.print_tsv_dictionary(fout)


def generate_additional_tokens(fname, basename, suffix, translator, additional_tokens):
    # print("==== Start generate additional token ====")
    # print("\n".join(str(a) for a in additional_tokens))
    additional_tokens_activity1_fname_tsv = "../out/" + basename + suffix + "_1_additional_tokens.tsv"
    additional_tokens_activity2_fname_tsv = "../out/" + basename + suffix + "_2_additional_tokens.tsv"
    print("in: ", fname, "out (tsv dicts for additional tokens):",
          additional_tokens_activity1_fname_tsv, additional_tokens_activity2_fname_tsv)

    with open(additional_tokens_activity1_fname_tsv, "w") as fout_1, \
        open(additional_tokens_activity2_fname_tsv, "w") as fout_2:
        translator.print_tsv_dictionary(fout_1, translate_only=[x.text for x in additional_tokens if x.flag1])
        translator.print_tsv_dictionary(fout_2, translate_only=[x.text for x in additional_tokens if x.flag2])
        # translator.print_tsv_dictionary(fout, translate_only=additional_tokens)

    header = open("../in/header_tokens.tex").readlines()
    footer = open("../in/footer_tokens.tex").readlines()

    with open("../tmp/" + basename + suffix + "_cards_activity_1.tex", "w") as fout, \
            open("../tmp/" + basename + suffix + "_cards_activity_1_t.tex", "w") as fout_t:

        print("\n".join(header), file=fout)
        print("\n".join(header), file=fout_t)
        for token in additional_tokens:
            if token.flag1:
                for _ in range(token.repetitions):
                    print("\\begin{vplace} \\textit{"+token.text+" }\\end{vplace} \\clearpage", file=fout)
                    print("\\begin{vplace}" + translator.translate(token.text) + "\\end{vplace} \\clearpage", file=fout_t)

        print("\n".join(footer), file=fout)
        print("\n".join(footer), file=fout_t)

    with open("../tmp/" + basename + suffix + "_cards_activity_2.tex", "w") as fout, \
            open("../tmp/" + basename + suffix + "_cards_activity_2_t.tex", "w") as fout_t:

        print("\n".join(header), file=fout)
        print("\n".join(header), file=fout_t)
        for token in additional_tokens:
            if token.flag2:
                for _ in range(token.repetitions):
                    print("\\begin{vplace} \\textit{" + token.text + "} \\\\ \\vspace{1cm} " + token.pos +
                          " \\end{vplace} \\clearpage", file=fout)
                    print("\\begin{vplace} " + translator.translate(token.text) + "\\\\ \\vspace{1cm} " +
                          translator.translate_pos(token.pos) + " \\end{vplace} \\clearpage", file=fout_t)

        print("\n".join(footer), file=fout)
        print("\n".join(footer), file=fout_t)

    compile_pdf("../tmp/" + basename + suffix + "_cards_activity_1_t.tex", do_copy=False)
    compile_pdf("../tmp/" + basename + suffix + "_cards_activity_1.tex", do_copy=False)
    compile_pdf("../tmp/" + basename + suffix + "_cards_activity_2_t.tex", do_copy=False)
    compile_pdf("../tmp/" + basename + suffix + "_cards_activity_2.tex", do_copy=False)

    for a in ["1", "2"]:
        fn_inp = "../tmp/" + basename + suffix + "_cards_activity_" + a + ".pdf"
        fn_odd = "../tmp/" + basename + suffix + "_cards_activity_" + a + ".odd.pdf"
        fn_even = "../tmp/" + basename + suffix + "_cards_activity_" + a + ".even.pdf"
        fn_shuf = "../tmp/" + basename + suffix + "_cards_activity_" + a + ".shuf.pdf"
        fn_shuf_t = "../tmp/" + basename + suffix + "_cards_activity_"+ a +"_t.pdf"
        fn_nup = "../tmp/" + basename + suffix + "_cards_activity_" + a + ".shuf-2x2.pdf"
        fn_nup_t = "../tmp/" + basename + suffix + "_cards_activity_" + a + "_t-2x2.pdf"
        fn_out = "../out/" + basename + suffix + "_cards_activity_" + a + ".pdf"

        print ("in: ",fn_inp, " and ", fn_shuf_t, "out (cards)", fn_out )

        os.system ("pdftk A='"+fn_inp+"' cat Aodd output '"+fn_odd+"'")
        os.system ("pdftk A='"+fn_inp+"' cat Aeven output '"+fn_even+"'")
        os.system ("pdftk A='"+fn_even+"' B='"+fn_odd+"' shuffle A B output '"+fn_shuf+"'")
        os.system ("pdfnup -q --nup 2x2 --suffix '2x2' --batch '"+fn_shuf+"' '"+fn_shuf_t+"' --outfile ../tmp")
        os.system ("pdftk A='"+fn_nup+"' B='"+fn_nup_t+"' shuffle A B output '"+fn_out+"'")
        
    # print("==== END generate additional token ====")


def generate_all_the_dings(fname, basename, suffix, translator):
    dings_fname = "../tmp/" + basename + suffix + "_all_the_dings.tex"
    print("in: ", fname, "out (dings):", dings_fname)

    with open(dings_fname, "w") as fout:
        with open("../in/header_dict.tex") as headerin:
            print(headerin.read(), file=fout)

        for x in sorted(_DINGS):
            print("\\ding{" + str(x) + '}\n\n', file=fout)

        with open("../in/footer_dict.tex") as footerin:
            print(footerin.read(), file=fout)

    compile_pdf(dings_fname)


# parses one or more tsv files and converts them to latex, then compiles the latex sources and outputs a pdf version
def main(fnames, dings=False, also_translate_files=[]):
    additional_tokens = []
    for f in also_translate_files:
        additional_tokens.extend(parse_additional_tokens(f))

    for fname in fnames:

        if fname.split(".")[-2].endswith('it'):
            # ITALIAN
            poslist = []
        else:
            # ENGLISH
            poslist = ["P", "V", "A", "E", "R", "Z", "S", "D", "B", "C"]

        if dings:
            suffix = "_dings"
            translator = TranslatorDings(_DINGS, poslist)
        else:
            suffix = "_letters"
            translator = TranslatorLetters(_DINGS, poslist)

        for tok in additional_tokens:
            translator.translate(tok.text)
            translator.translate_pos(tok.pos)

        basename = "".join(os.path.basename(fname).split(".")[:-1])

        generate_translate_text_and_grammar_and_corpus(fname, basename, suffix, translator)

        generate_dict(fname, basename, suffix, translator)

        if additional_tokens:
            generate_additional_tokens(fname, basename, suffix, translator, additional_tokens)

        if dings:
            generate_all_the_dings(fname, basename, suffix, translator)


def parse_command_line_arguments(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument("infiles", nargs="+", metavar="input.tsv",
                        help="tsv file(s) to convert")
    parser.add_argument("-d", "--dings", action="store_true",
                        help="translate text to dings instead of letters")
    parser.add_argument("-a", "--also-translate", nargs=1, metavar="file", default=[],
                        help="also translate the tokens read from this file (one token per line)")
    return parser.parse_args(args)


if __name__ == "__main__":
    args = parse_command_line_arguments()
    print("command line arguments:", args)

    os.makedirs("../out", exist_ok=True)
    os.makedirs("../tmp", exist_ok=True)

    main(args.infiles, args.dings, args.also_translate)
