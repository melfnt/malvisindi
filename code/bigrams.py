'''
	usage:
		python bigrams.py input_file.tsv
	
	given an input tsv file computes the bigrams of the text and prints them sorted by frequency on an output tsv file.
	the output file is saved into ../out

	output format:
		# lines started with # are comments
		# one line per bigram, each line has three columns separated by \t: word1\tword2\tfrequency
		# such that word1, word2 is a bigram in the corpus
		# for example:
		<sos>/    the/R    12
		the/R    queen/S   9
		<sos>/    snowwhite/S      8
		in/E    the/R      7
		<sos>/    she/P    7
		beautiful/A    unlike/E    7
		# and so on...
		# <sos>, <eos> mean start of sentence, end of sentence respectively

	the output file can be used to generate new text using the script generate_text.py

'''

import sys
import os
from collections import Counter

def compute_bigrams ( A ):
	x = None
	for y in A:
		if x is not None:
			yield (x,y)
		x = y

def main ( in_fname ):

	out_fname = "../out/" + os.path.basename(in_fname).replace (".tsv", "") + "_bigrams.tsv"
	print ("in: {}, out: {}".format (in_fname, out_fname))

	fd = Counter ()

	with open(in_fname) as fin:
		prevline = []
		prevprevline = []
		for lineno, line in enumerate(fin):
			line = line.strip().split('\t')
			
			if line[0].isdigit():
				if len(prevline) > 0:
					out = ['{}/{}'.format(x.lower(), y) for x, y in zip(['<SOS>']+prevline+['<EOS>'], ['']+prevprevline+[''])]
					fd.update (compute_bigrams (out))
					
					# DEBUG
					# if 'strega' in prevline:
					# 	print("sentence:", prevline)
					# 	print("token/pos:", out)
					# 	print("bigrams:", compute_bigrams(out))
					# 	print("bigrams counter up to now:", fd)
					# 	input()
			
			prevprevline = prevline
			prevline = line
		
		if len(prevline) > 0:
			out = ['{}/{}'.format(x.lower(), y) for x, y in zip(['<SOS>']+prevline+['<EOS>'], ['']+prevprevline+[''])]
			fd.update(compute_bigrams(out))

	with open(out_fname, "w") as fout:
		print ("# BIGRAMS SORTED BY FREQUENCY", file=fout)
		print ("# w1\tw2\tfrequency", file=fout)

		for x, y in sorted(fd.items(), key = lambda x: -x[1]):
			print("{}\t{}\t{}".format(x[0],x[1], y), file=fout)

if __name__ == "__main__":

	if len(sys.argv) < 2 or not os.path.isfile (sys.argv[1]):
		sys.exit ("No input file specified or input file is not a file. Please specify a tsv file")

	os.makedirs ("../out", exist_ok=True)

	main (sys.argv[1])
