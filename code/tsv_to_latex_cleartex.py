'''
	usage:
		python tsv_to_latex_cleartext.py input_file.tsv
	
    Given an input tsv file, parses the text and produces a latex file with the same text in the folder ../tmp
    The latex file in the ../tmp folder is compiled using lualatex and the output pdf file is put in the ../out folder.
    The latex file and the auxiliary files in ../tmp are not deleted so that the user can possibly to edit and recompile it manually.

'''

import sys
import os

#horizontal space between words (mm)
_HSKIP = 10
#vertical space between sentences (mm)
_VSKIP = 9

#do not break line after these sentences
_DO_NOT_BREAK = [5, 7, 16, 18, 22, 26, 29, 31, 33, 35, 37, 39, 48, 52, 54, 57, 61]

max_len_word = ""

# yelds sentence_id, sentence, is_trailing_newline
# sentence -> ["word1", "word2", "word3 possibly including spaces", "word4"]
def parse_tsv (fname):
    sent_id = 1
    with open(fname) as fin:
        prev_sentence = []
        for line in fin:
            line = line.rstrip ()
            # print ("new line (stripped)", line)
            row = line.split('\t')
            # print ("row", row)
            # print ("row[0]", row[0])
            if row[1] and len(prev_sentence):
                yield sent_id, prev_sentence, not sent_id in _DO_NOT_BREAK
                sent_id += 1
            prev_sentence = row[2:]

            global max_len_word
            max_len_word = max (prev_sentence + [max_len_word], key=lambda x: len(x))
        
        if len(prev_sentence):
            yield sent_id, prev_sentence, not sent_id in _DO_NOT_BREAK

def sentence_to_latex ( sent_id, sentence, newline ):
    hspace = " \\hspace{"+ str(_HSKIP) +"mm} "
    vspace = "\n\\vspace{"+ str(_VSKIP) +"mm}\n" if newline else "\\hspace{2cm}"
    return hspace.join ( sentence ) + "\n" + vspace + "\n"

# given the name of a tex file in the ../tmp folder compiles it and copies the pdf output in the ../out folder
def compile_pdf ( latex_fname ):
    created_pdf_fname = latex_fname.replace (".tex", ".pdf")
    output_fname = created_pdf_fname.replace ("../tmp/", "../out/")
    print ("compiling latex file {}. output: {}".format(latex_fname, output_fname))
    os.system ("lualatex -interaction=nonstopmode --output-directory=../tmp " + latex_fname + " | grep Overfull -A1")
    os.system ("cp '{}' '{}'".format (created_pdf_fname, output_fname))

#parses a tsv file and converts it to latex
def main ( fname ):

    suffix = "_cleartext"
    
    basename = os.path.basename (fname)

    out_fname = "../tmp/" + "".join(basename.split(".")[:-1]) + suffix + ".tex"
    print ("in: ", fname, "out (cleartext):", out_fname)

    with open (out_fname, "w") as fout:
        with open ("../in/header_cleartext.tex") as headerin:
            print (headerin.read(), file=fout)

        for sent_id, sentence, newline in parse_tsv (fname):
            # print (sentence)
            s = sentence_to_latex (sent_id, sentence, newline)
            print (s, file=fout, end='')
            
        
        with open ("../in/footer_cleartext.tex") as footerin:
            print (footerin.read(), file=fout)
    
    print ("Longest word: {} ({})".format(max_len_word, len(max_len_word)))
    compile_pdf ( out_fname )

if __name__ == "__main__":

    if len(sys.argv) < 2 or not os.path.isfile (sys.argv[1]):
        sys.exit ("No input file specified or input file is not a file. Please specify a tsv file")
    
    os.makedirs ("../out", exist_ok=True)
    os.makedirs ("../tmp", exist_ok=True)

    main (sys.argv[1])
